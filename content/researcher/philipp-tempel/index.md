---
title: "Philipp Tempel"
date: 2020-04-27T16:46:50+02:00
toc: true
position: Postdoctoral Research Associate
organization:
    - tudelft-pme
location: Delft, the Netherlands
website: https://philipptempel.me
social:
 - link: https://gitlab.com/philipptempel
   icon: gitlab
   icon_pack: fe
 - link: https://github.com/philipptempel
   icon: github
   icon_pack: fe
 - link: https://instagram.com/phlpptmpl
   icon: instagram
   icon_pack: fe
 - link: https://scholar.google.com/citations?user=lB9CAbQAAAAJ
   icon: google-scholar
   icon_pack: ai
links:
  - link: https://google.com
    icon: google
    icon_pack: fab
resources:
  - name: cover
    src: cover.png
  - name: avatar
    src: avatar.png
---
After receiving his doctoral degree (Dr.-Ing.) in robotics from the University of Stuttgart in July 2019, Philipp joined Just Herder’s group at the department of Precision and Microsystems Engineering in October 2019.
He received his diploma (Dipl.-Ing.) in Engineering Cybernetics at the university of Stuttgart in 2013, after which he joined Andreas Pott's group at the Institute for Control Engineering of Machine Tools and Manufacturing Units ISW at the University of Stuttgart.
In 2014, he was a visiting researcher with Prof. Jong-Oh Park at the Joint Robotics Laboratory, Robot Research Initiative at Chonnam National University in Gwangju, South Korea, and in 2016 he was with Marc Gouttefarde of Team DEXTER at LIRMM (Laboratoire d’Informatique, de Robotique et de Microélectronique de Montpellier; Laboratory of Computer Science, Robotics and Microelectronics of Montpellier).

His research topics are kinematics and dynamics of flexible robots, more particularly cable-driven parallel robots.
These systems comprise elastic and flexible cables for actuation of a rigid end effector which results in bodies with differences in stiffness of multiple magnitudes.
Analysis and simulation of cable robots requires different tools than known from rigid member robots like Gough-Stewart platforms or Flexpickers.

## Research Interests

* Cable-driven parallel robots
* Flexible robots
* Multibody dynamics
* Numerical and mechanical integrators

## Projects: 

### Active Projects

#### CaRISA
A Cable-Driven Parallel Robot for Inspection and Scanning of Artwork.
See the [robot's profile](/robot/carisa) on this website.

### Previous Projects

#### Expo 2015
I designed, validated, and commissioned two cable robots for the German pavilion at the World Expo 2015 in Milan, Italy.

#### Dynamics of Cable-Driven Parallel Robots with Elastic and Flexible Cables of Time-Varying Length
I extended the kinematic and dynamic formulation of cable robots using Cosserat Rod theory for modeling the cables.

#### EndlessZ
I co-supervised a PhD student at the University of Stuttgart on kinematic modeling and simulation of cable robots with multiple platforms and an endless rotation about one axis.
