---
title: "Philipp Miermeister"
date: 2020-07-06T09:06:30+02:00
toc: true
organization:
    - mpi-cyb
location: Tübingen, Germany
social:
  - link: https://www.linkedin.com/in/philipp-miermeister-4838b529/
    icon: linkedin
    icon_pack: fe
links:
  - link: http://www.cablerobotsimulator.org/
  - link: https://www.cyberneum.de/
resources:
   - name: cover
     src: cover.png
   - name: avatar
     src: avatar_.png
---
Philipp Miermeister works on model selection and optimization for cable-driven parallel robots and designed the CableRobot Simulator which is operated at the
Max Planck Institute for Biological Cybernetics research facilities.
