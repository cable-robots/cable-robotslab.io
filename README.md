![Build Status](https://gitlab.com/cable-robots/cable-robots.gitlab.io/badges/master/pipeline.svg) ![License: MIT](https://img.shields.io/badge/license-MIT-green.svg)

---

**Table of Contents**

[[_TOC_]]

---



## GitLab CI

This project's static Pages are built by [GitLab CI][gitlab-ci], following the steps
defined in [`.gitlab-ci.yml`](.gitlab-ci.yml).

## Requirements

To review, change, and amend the website locally, you will need a few tools installed

* [git][git] of course for cloning and tracking changes
* [git lfs][git-lfs] for tracking all binary files inside the `content/` directory i.e., images, videos, etc.
* [hugo extended][hugo-extended] to render the website locally (not needed but useful)

## Building locally

To see the project locally, you'll have to follow the steps below:

1. Fork, clone, or download this project
1. Preview the project: `hugo server`

Read more at Hugo's [documentation][hugo-documentation].

### Preview your site

If you clone or download this project to your local computer and run `hugo server`,
your site can be accessed under `localhost:1313/`.

## Contributing

To make changes or add content, follow these easy steps

1. Fork the repository (depending on your project access level you can only fork)
1. Clone the/your repository
1. Turn on git lfs by executing `git lfs install`
1. Create a new branch for your changes and switch to it `git checkout -b <your-self-explanatory-branch-name>`
1. Make your changes and commit them
   *. Keep in mind that any binary image or video file (see the `.gitattributes` file for a list) must be added as `git lfs` object
1. Push your changes to the remote
1. Create a merge request (MR)
   1. Your MR must source from `your-self-explanatory-branch-name` and target to the `master` branch on the repository at `https://gitlab.com/cable-robots/cable-robots.gitlab.io`
   1. Resolve all discussions with the maintainers and developers
   1. Once all discussions are resolved, a maintainer or developer will allow your MR to be merged and published publicly
1. If need be: go back to 4.

## Troubleshooting

1. CSS is missing! That means two things:

    Either that you have wrongly set up the CSS URL in your templates, or
    your static generator has a configuration option that needs to be explicitly
    set in order to serve static assets under a relative URL.
2. Receiving syntax errors on clean build:
   
    It is very likely that you have installed the default version of `hugo` and
    not the extended version. We require the extended version due to CSS/JS
    minification, image processing, and resource contatenation taking place
    during building of the site. Standard `hugo` cannot do these taks and fails
    with cryptic syntax error messages.

[gitlab-ci]: https://about.gitlab.com/gitlab-ci/

[hugo]: https://gohugo.io
[hugo-extended]: https://gohugo.io/overview/installing/
[hugo-documentation]: https://gohugo.io/overview/introduction/
[git]: https://git-scm.com
[git-lfs]: https://git-lfs.github.com