---
title: "Thomas Reichenbach"
date: 2020-07-21T07:00:00+02:00
toc: true
organization:
    - ustutt-isw
location: Stuttgart, Germany
social:
  - link: https://www.researchgate.net/profile/Thomas_Reichenbach
    icon: researchgate
    icon_pack: ai
  - link: https://www.linkedin.com/in/thomas-reichenbach-67925b134/
    icon: linkedin
    icon_pack: fe
  - link: https://www.xing.com/profile/Thomas_Reichenbach14/
    icon: xing
    icon_pack: fab
resources:
   - name: cover
     src: cover.jpg
   - name: avatar
     src: avatar.jpg
---
Thomas Reichenbach is a research associate in cable-driven parallel robots at the Institute for Control Engineering of Machine Tools and Manufacturing Units (ISW), University of Stuttgart (Germany).
His research interests include cable drive systems, parallel robots, and kinematics.
