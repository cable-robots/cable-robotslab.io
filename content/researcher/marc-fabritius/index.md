---
title: "Marc Fabritius"
date: 2020-05-07T16:46:50+02:00
toc: true
organization:
    - fhg-ipa
location: Stuttgart, Germany
social:
  - link: https://www.researchgate.net/profile/Marc_Fabritius
    icon_pack: ai
    icon: researchgate
resources:
   - name: cover
     src: cover.jpg
   - name: avatar
     src: avatar.png
---
Marc Fabritius is a research associate in cable-driven parallel robots at the Fraunhofer Institute for Manufacturing Engineering and Automation IPA.
His research interests include parallel robots, kinematics, applied mathematics and simulation.
