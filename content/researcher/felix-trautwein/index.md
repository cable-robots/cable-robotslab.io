---
title: "Felix Trautwein"
date: 2020-07-15T15:44:50+02:00
toc: true
organization:
    - ustutt-isw
location: Stuttgart, Germany
resources:
   - name: cover
     src: cover.jpg
   - name: avatar
     src: avatar.jpg
---
Felix is a research assistant at the Institute for Control Engineering of Machine Tools and Manufacturing Units of the University of Stuttgart.
His research interest include reconfiguration of cable robots.
