---
title: "Christoph Martin"
date: 2020-06-30T16:46:50+02:00
toc: true
organization:
    - fhg-ipa
location: Stuttgart, Germany
resources:
   - name: cover
     src: cover.jpg
   - name: avatar
     src: avatar.jpg
---
Christoph Martin is a research associate in cable-driven parallel robots at the Fraunhofer Institute for Manufacturing Engineering and Automation IPA.
His research interests include parallel robots, kinematics, control, design and hardware.