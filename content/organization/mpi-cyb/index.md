---
title: Max Planck Institute for Biological Cybernetics
date: 2020-05-07T17:49:57+02:00
short_title: MPI BioCyb
institution: Max-Planck-Gesellschaft
short_institution: MPG
location: Tübingen, Germany
website: https://www.mpg.de/152075/biologische-kybernetik
resources: 
    - name: cover
      src: cyberneum.jpg
    - name: avatar
      src: avatar.jpg
---

The Max Planck Institute for Biological Cybernetics investigates information processing in the brains of humans.
Experimental and theoretical methods as well as computer simulations help to investigate the processes that translate sensory stimuli into perceptions and memories, and allow us to make decisions and act.
The research also focuses on the interaction of different senses, the impact of the spatial environment on behavior, and the interaction of perception and action which is done with various motion simulators such as the CableRobot simulator in the Cyberneum facility.

The CableRobot simulator provides a novel approach to the design of motion simulation platforms in as far as it uses cables and winches for actuation instead of rigid links known from hexapod simulators.
This approach allows reducing the actuated mass, scale up the workspace significantly, and provides great flexibility to switch between system configurations in which the robot can be operated.
The simulator is used for studies in the field of human perception research, flight simulation, and virtual reality applications.
