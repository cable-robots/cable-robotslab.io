---
title: "HEPHAESTUS Cable Robot"
date: 2020-04-27T18:16:58+02:00
long_title: "Highly automatEd PHysical Achievements and performancES using cable roboTs Unique Systems"
description: "A cable robot developed for the construction industry with the application of facade installation"
motionpattern: 3r3t
organization: fhg-ipa
resources:
  - src: 'gallery/DSC03694.jpg'
    title: 'HEPHAESTUS Cable Robot'
    params:
      description: "Full view of the HEPHAESTUS Cable Robot installed in front of a three-story demo building"
  - src: 'gallery/IMG_3700.jpg'
    title: 'HEPHAESTUS cable robot platform #1'
    params:
      description: "Close shot of the cable robot platform which can automatically install brackets and mount facade panels."
  - src: 'gallery/DSC03685.jpg'
    title: 'HEPHAESTUS cable robot platform #2'
    params:
      description: "Side view of the cable robot platform"
  - src: 'gallery/DSC03662.jpg'
    title: 'HEPHAESTUS crane'
    params:
      description: "Medium close-up of one of the two cranes which house the pulleys that guide the upper cables of the the HEPHAESTUS Cable Robot"
links:
    - url: https://www.hephaestus-project.eu
      title: Project website
---
A cable robot developed for the construction industry with the application of facade installation.

{{< youtube FmpbLwmz2OY >}}
