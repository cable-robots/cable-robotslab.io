---
title: "HINDCON CDPR"
date: 2020-04-27T18:16:58+02:00
long_title: "Hybrid INDustrial CONstruction Cable Robot"
description: "A cable robot developed for additive and subtractive manufacturing of concrete"
motionpattern: 3r3t
organization: fhg-ipa
resources:
    - src: 'gallery/add1.jpg'
      title: 'additive manufacturing #1'
      params:
        description: "The HINDCON Cable Robot is printing a workpiece."
    - src: 'gallery/add2.jpg'
      title: 'additive manufacturing #2'
      params:
        description: "The HINDCON Cable Robot is printing a workpiece."
    - src: 'gallery/add3.jpg'
      title: 'additive manufacturing #3'
      params:
        description: "The HINDCON Cable Robot is printing a workpiece."
    - src: 'gallery/sub1.jpg'
      title: 'subtractive manufacturing #1'
      params:
        description: "The HINDCON Cable Robot is performing subtractive finishing tasks on a previously printed workpiece."
    - src: 'gallery/sub2.jpg'
      title: 'subtractive manufacturing #2'
      params:
        description: "The HINDCON Cable Robot is performing subtractive finishing tasks on a previously printed workpiece."
links:
    - url: https://www.hindcon3d.com/
      title: Project website
---
A cable robot developed for additive and subtractive manufacturing of concrete.

ATANGA Hindcon - Very Big 3D Printing with Concrete:

{{< youtube dN1pNz1OlC0 >}}

Final Project Video:

{{< youtube PZPbd5WC6lQ >}}

