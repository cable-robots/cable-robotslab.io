---
title: "{{ replace .Name "-" " " | title }}"
date: {{ .Date }}
superseded: false
draft: true
long_title: Cable Robot for Inspection and Scanning of Art objects
description: "Mounted with various imaging devices like UV/IR cameras and spectrometer, this robot aims to scan paintings with high accuracy."
motionpattern:
    - 2r3t
    - 3r3t
organization:
    - tudelft-pme
    - lirmm
---

**Insert Lead paragraph here.**

## Documentation

### `motionpattern`

To assign the robot one or more motion patterns, simply add them to the front matter.
This way, the robot will be showin the respective motion pattern page automatically.

#### Sample
```yaml
motionpattern:
    - 1r2t
    - 3r3t
``` 

where the entries in the array of `motionpattern` must correspond to the name of a motion pattern in `content/motionpattern` i.e., a directory name of a file name (except for `_index.md`).


### `organization`

Assign the robot to an organization by simply putting its slug (i.e., directory name or file name under `/content/organization/`) in this field.
A robot can be assigned more than one organization, and these will be rendered in the order they are defined in `organization`.

#### Sample
```yaml
organization:
    - tudelft-pme
    - lirmm
```

### `long_title`

A robot is very often referred to by its short name.
However, it may be favorable to also provide the long name of the robot.
Just add the robot's long name in this field

#### Sample
```yaml
long_title: Cable Robot for Inspection and Scanning of Art objects
```


### `superseded`

Technology advances and some robots may be outdated and superseded by others.
If this is the case, then you should set this parameter to the name of the superseding robot.
The value can be either the human readable name of the robot or its `urlize`'d version.

#### Sample
```yaml
superseded: CaRISA
```


### `resources`

Want to add resources like files, images, avatar, or a cover image to the robot?
These go into this section.

#### Cover Image

By convention, each page can have a cover image that shows whatever you want it to show.
Make sure your image content fits into a 1200x200 image as these are the fixed dimensions of the cover image.
Two field names must be present for the resource to be identified as a cover image.
These are `name` with value `cover` and `src` with the image source relative to the robot's directory.

##### Sample
```yaml
resources:
    - name: cover
      src: "cover.jpg"
```

#### Avatar

An avatar is a small depiction of the robot which is used in various places on the website.
Pick something that makes this robot stand out from the other ones and that is easily identifiable.
All avatars are rendered with square side dimensions (in fact, the base dimensions are 600x600), so make sure that your avatar fits this shape.
Two field names must be present for the resource to be identified as an avatar.
These are `name` with value `avatar` and `src` with the image source relative to the robot's directory.

##### Sample
```yaml
resources:
    - name: avatar
      src: "avatar.jpg"
```


#### Gallery

What is a portfolio without images?
We implemented a small gallery feature that you can use to show case images of your robot or associated images.
For you to add a gallery feature to your robot, just create a directory `gallery` in the robot's directory and add all images you want to add.
Be aware, initially the file name will be used as image title in the gallery.
You can change this by adding resource meta information to the front matter.
Similar to the avatar and cover image, two fields are needed to define the meta information.
These are `src` pointing to the image file and `title` denoting the image title to be used instead of the file name.
To protect your intellectual property, add the `params.copyright` parameter with a copyright of your image.

##### Sample
```yaml
resources:
    - src: 'gallery/image-01.jpg'
      title: 'This is the first image of my robot'
      params:
        copyright: Copyright some authors.
```


#### Files

Besides a gallery, you may also add files for download to the robot.
We implemented a small tabular display of files on the robot page.
For you to add this feature to your robot, just create a directory `files` in the robot's directory and add all files you want to add.
Be aware, initially the file name will be used as file name in the overview.
You can change this by adding resource meta information to the front matter.
Similar to the avatar and cover image, two fields are needed to define the meta information.
These are `src` pointing to the data file and `title` denoting the data file title to be used instead of the file name.
By default, each file will be display with a `file` icon, however, you can change this behavior by adding the `params.icon` entry.
A list of supported icons can be found at [https://feathericons.com](Feathericons)

##### Sample
```yaml
resources:
    - src: 'files/robot.json'
      title: 'Robot JSON file'
      params:
        icon: file
```
