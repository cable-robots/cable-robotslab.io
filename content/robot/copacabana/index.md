---
title: "COPacabana"
date: 2020-04-27T18:16:58+02:00
long_title: "Cable OPerated Robot w/ 16 Cables"
description: "A cable robot with up to 16 cables used for reconfiguration and pick-and-place with endless rotation."
motionpattern: 3r3t
organization: ustutt-isw
resources:
    - name: cover
      src: 'cover.jpg'
    - src: 'gallery/IMG_0571.jpg'
      title: 'Winch system'
      params:
        copyright: ISW, University of Stuttgart
    - src: 'gallery/IMG_7445_bearb.jpg'
      title: 'Platform'
      params:
        copyright: ISW, University of Stuttgart
    - src: 'gallery/IMG_7561_bearb.jpg'
      title: 'Platform, frame and winch systems'
      params:
        copyright: ISW, University of Stuttgart
    - src: 'gallery/IMG_7329.JPG'
      title: 'Frame calibration'
      params:
        copyright: ISW, University of Stuttgart
    - src: 'gallery/IMG_7588.JPG'
      title: 'Winch assembly on frame'
      params:
        copyright: ISW, University of Stuttgart
    - src: 'gallery/IMG_7551_bearb2.jpg'
      title: 'Assembly view #1 of COPacabana'
      params:
        copyright: ISW, University of Stuttgart
    - src: 'gallery/IMG_7552.jpg'
      title: 'Assembly view #2 of COPacabana'
      params:
        copyright: ISW, University of Stuttgart
    - src: 'gallery/IMG_7347.jpg'
      title: 'Assembly of pulley guiding mechanism'
      params:
        copyright: ISW, University of Stuttgart        
    - src: 'gallery/IMG_8428.jpg'
      title: 'Endless-Z #1'
      params:
        copyright: ISW, University of Stuttgart    
    - src: 'gallery/IMG_8393.jpg'
      title: 'Endless-Z #2'
      params:
        copyright: ISW, University of Stuttgart    
    - src: 'gallery/IMG_8419.jpg'
      title: 'Endless-Z #3'
      params:
        copyright: ISW, University of Stuttgart                    
---
Initially two separate robots, now a single robot with up to 16 synchronized axes and endless rotation of the platform.

