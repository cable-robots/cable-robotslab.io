---
title: Precision and Microsystems Engineering
date: 2020-04-27T16:54:33+02:00
short_title: PME
institution: Delft University of Technology
short_institution: TU Delft
location: Delft, the Netherlands
website: http://pme.tudelft.nl
resources:
    - name: cover
      src: cover.jpg
---
The Department of Precision and Microsystems Engineering (PME) carries out research and provides education in the field of high-tech systems and scientific instrumentation.
We aim to solve fundamental questions in engineering science to advance the performance of precision systems and devices as well as their design.
Our specific mission is to merge mechanical engineering concepts with micro and nanoscience.
We deliver skilled professionals and contribute knowledge to support leadership of the high-tech systems industry.
