---
title: "{{ replace .Name "-" " " | title }}"
date: {{ .Date }}
draft: true
toc: true
organization:
    - tudelft-pme
    - ustutt-isw
location: 'Where?'
website: https://philipptempel.me
social:
   - link: https://github.com/philipptempel
     icon_pack: fe
     icon: github
   - link: https://instagram.com/phlpptmpl
     icon_pack: fe
     icon: instagram
   - link: https://scholar.google.com/citations?user=lB9CAbQAAAAJ
     icon_pack: ai
     icon: google-scholar
links:
   - link: https://google.com
     icon_pack: fab
     icon: google
---

**Insert Lead paragraph here.**



## Documentation

### `organization`

Assign the researcher to an organization by simply putting its slug (i.e., directory name or file name under `/content/organization/`) in this field.
A researcher can be assigned more than one organization, and these will be rendered in the order they are defined in `organization`.

#### Sample
```yaml
organization:
    - tudelft-pme
    - lirmm
```


### `location`

Where the researcher lives.
Is a markdown enabled field.

#### Sample
```yaml
location: Delft, the Netherlands
```


### `website`

Address of either a personal or professional website of the researcher.

#### Sample
```yaml
website: 'https://philipptempel.me'
```


### `social`

If the researcher has any social links they would like to show, you can add them here.
The field is a simple list of mappings, each mapping containing the `icon` and `link` field.
Field `link` defines the link to the remote page, field `icon_pack` any of the valid icon packs listed below (or defaults to `fe` if not given), and `icon` is a valid icon name from the list of supported icons by that icon pack (**do not include the icon pack's prefix in the icon name*).
These are the four different icon packs:

  * `fe`: includes the basic icons on this website. [see all icons](https://feathericons.com)
  * `ai`: includes academic icons such as cv, google-scholar, arxiv, orcid, researchgate, mendeley. [see all icons](https://jpswalsh.github.io/academicons/)
  * `fab`: includes brand icons such as twitter, weixin, weibo, linkedin, github, facebook, pinterest, twitch, youtube, instagram, soundcloud. [see all icons](https://fontawesome.com/icons?d=gallery&s=brands)
  * `fas` or `far`: including general icons such as fax, envelope (for email), comments (for discussion forum). [see all icon](https://fontawesome.com/icons?d=gallery&s=regular,solid)

#### Sample
```yaml
social:
  - link: https://gitlab.com/cable-robots
    icon: gitlab
    icon_pack: fe
  - link: https://google.com
    icon: external-link
    icon_pack: fe
```


### `links`

If you want several links displayed on your profile's short summary, you can add them to this section.
Syntax is the same as for entries in `social`, except you put the under the `links` section.

#### Sample
```yaml
links:
  - link: https://google.com
    icon: google
    icon_pack: fab
  - link: https://example.com
    icon: external-link
    icon_pack: fe
``` 


### `resources`

Want to add resources like files, images, avatar, or a cover image to the profile?
These go into this section.

#### Cover Image

By convention, each page can have a cover image that shows whatever you want it to show.
Make sure your image content fits into a 1200x200 image as these are the fixed dimensions of the cover image.
Two field names must be present for the resource to be identified as a cover image.
These are `name` with value `cover` and `src` with the image source relative to the profile's directory.

##### Sample
```yaml
resources:
  - name: cover
    src: "cover.jpg"
```

#### Avatar

An avatar is a small depiction of the researcher which is used in various places on the website.
Pick something that makes this researcher stand out from the other ones and that is easily identifiable.
All avatars are rendered with square side dimensions (in fact, the base dimensions are 600x600), so make sure your avatar fits this shape.
Two field names must be present for the resource to be identified as an avatar.
These are `name` with value `avatar` and `src` with the image source relative to the researcher's directory.

##### Sample

```yaml
resources:
  - name: avatar
    src: "avatar.jpg"
```
